import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SwipRoutingModule } from './swip-routing.module';
import { SwipComponent } from './swip.component';


@NgModule({
  declarations: [
    SwipComponent
  ],
  imports: [
    CommonModule,
    SwipRoutingModule
  ]
})
export class SwipModule { }
