import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DoctorCareComponent } from './doctor-care.component';

const routes: Routes = [{ path: '', component: DoctorCareComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DoctorCareRoutingModule { }
