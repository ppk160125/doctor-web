import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DoctorCareRoutingModule } from './doctor-care-routing.module';
import { DoctorCareComponent } from './doctor-care.component';


@NgModule({
  declarations: [
    DoctorCareComponent
  ],
  imports: [
    CommonModule,
    DoctorCareRoutingModule
  ]
})
export class DoctorCareModule { }
