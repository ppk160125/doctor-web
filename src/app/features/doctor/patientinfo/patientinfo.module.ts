import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PatientinfoRoutingModule } from './patientinfo-routing.module';
import { PatientinfoComponent } from './patientinfo.component';
import { NgZorroModule } from 'src/app/ng-zorro.module';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [
    PatientinfoComponent
  ],
  imports: [
    CommonModule,
    NgZorroModule,
    SharedModule,
    PatientinfoRoutingModule

  ]
})
export class PatientinfoModule { }
