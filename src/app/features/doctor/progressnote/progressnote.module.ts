import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProgressnoteRoutingModule } from './progressnote-routing.module';
import { ProgressnoteComponent } from './progressnote.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgZorroModule } from 'src/app/ng-zorro.module';


@NgModule({
  declarations: [
    ProgressnoteComponent
  ],
  imports: [

    NgZorroModule,
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    ProgressnoteRoutingModule
  ]
})
export class ProgressnoteModule { }
