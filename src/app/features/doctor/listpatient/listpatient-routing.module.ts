import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListpatientComponent } from './listpatient.component';

import {MainComponent} from './main/main.component';



const routes: Routes = [
  {
    path: '',
    component: ListpatientComponent,
    data: {
      breadcrumb: 'List Patient'
    },
    children: [
      {
        path: '',
        component: MainComponent,
        data: {
          breadcrumb: 'List Patient'
        },
      },
     
      {
        path: 'doctor-order',
        loadChildren: () => import('../doctor-order/doctor-order.module').then(m => m.DoctorOrderModule),
        data: {
          breadcrumb: 'Doctor Order'
        },
      },
      {
        path: 'patient-info',
        loadChildren: () => import('../patientinfo/patientinfo.module').then(m => m.PatientinfoModule),
        data: {
          breadcrumb: 'Patient Info'
        },
      },
      {
        path: 'progress-note',
        loadChildren: () => import('../progressnote/progressnote.module').then(m => m.ProgressnoteModule),
        data: {
          breadcrumb: 'Progress Note'
        },
      },
      {
        path: 'admission-note',
        loadChildren: () => import('../admissionnote/admissionnote.module').then(m => m.AdmissionnoteModule),
        data: {
          breadcrumb: 'Admission Note'
        },
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListpatientRoutingModule { }
