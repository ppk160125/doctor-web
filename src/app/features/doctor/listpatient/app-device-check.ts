import { Component, HostListener, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-device-check',
  template: '<p>Device Type: {{ deviceType }}</p><p>Resolution: {{ resolution }}</p>',
})
export class DeviceCheckComponent implements OnInit {
  deviceType: string = 'Unknown';
  resolution: string = 'Unknown';
  screenWidth: number = window.innerWidth;
  screenHeight: number = window.innerHeight;
  @Output() newItemEvent = new EventEmitter<string>();

  constructor(private breakpointObserver: BreakpointObserver) {}

  ngOnInit(): void {
   
    this.breakpointObserver.observe([
      Breakpoints.XSmall,
      Breakpoints.Small,
      Breakpoints.Medium,
      Breakpoints.Large,
    ]).subscribe(result => {
      if (result.matches) {
        if (result.breakpoints[Breakpoints.XSmall]) {
          this.deviceType = 'xs';
          this.resolution = 'w:'+this.screenWidth+'  h:'+this.screenHeight;
        } else if (result.breakpoints[Breakpoints.Small]) {
          this.deviceType = 'sm';
          this.resolution = 'w:'+this.screenWidth+'  h:'+this.screenHeight;
        } else if (result.breakpoints[Breakpoints.Medium]) {
          this.deviceType = 'md';
          this.resolution = 'w:'+this.screenWidth+'  h:'+this.screenHeight;
        } else if (result.breakpoints[Breakpoints.Large]) {
          this.deviceType = 'lg';
          this.resolution = 'w:'+this.screenWidth+'  h:'+this.screenHeight;
        }else{
            this.deviceType = 'xl';
          this.resolution = 'w:'+this.screenWidth+'  h:'+this.screenHeight;
        }
      }
    });
    this.addNewItem( this.deviceType);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.screenWidth = event.target.innerWidth;
    this.screenHeight = event.target.innerHeight;
    this.resolution = 'w:'+this.screenWidth+'  h:'+this.screenHeight;
  }
  addNewItem(value: string) {
    this.newItemEvent.emit(value);
  }
}
