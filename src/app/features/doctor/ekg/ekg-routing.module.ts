import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EkgComponent } from './ekg.component';

const routes: Routes = [{ path: '', component: EkgComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EkgRoutingModule { }
