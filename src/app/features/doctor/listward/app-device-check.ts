import { Component, HostListener, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-device-check',
  template: '<p>Device Type: {{ deviceType }}</p><p>Resolution: {{ resolution }}</p>',
})
export class DeviceCheckComponent implements OnInit {
  deviceType: string = 'Unknown';
  resolution: string = 'Unknown';
  screenWidth: number = window.innerWidth;
  screenHeight: number = window.innerHeight;
  @Output() newItemEvent = new EventEmitter<string>();

//   Breakpoint name 	Media query
// XSmall 	(max-width: 599.98px)
// Small 	(min-width: 600px) and (max-width: 959.98px)
// Medium 	(min-width: 960px) and (max-width: 1279.98px)
// Large 	(min-width: 1280px) and (max-width: 1919.98px)
// XLarge 	(min-width: 1920px)
// Handset 	(max-width: 599.98px) and (orientation: portrait), (max-width: 959.98px) and (orientation: landscape)
// Tablet 	(min-width: 600px) and (max-width: 839.98px) and (orientation: portrait), (min-width: 960px) and (max-width: 1279.98px) and (orientation: landscape)
// Web 	(min-width: 840px) and (orientation: portrait), (min-width: 1280px) and (orientation: landscape)
// HandsetPortrait 	(max-width: 599.98px) and (orientation: portrait)
// TabletPortrait 	(min-width: 600px) and (max-width: 839.98px) and (orientation: portrait)
// WebPortrait 	(min-width: 840px) and (orientation: portrait)
// HandsetLandscape 	(max-width: 959.98px) and (orientation: landscape)
// TabletLandscape 	(min-width: 960px) and (max-width: 1279.98px) and (orientation: landscape)
// WebLandscape 	(min-width: 1280px) and (orientation: landscape)

// ng zoorow
// {
//   xs: '575px',
//   sm: '576px',
//   md: '768px',
//   lg: '992px',
//   xl: '1200px',
//   xxl: '1600px'
// }

  constructor(private breakpointObserver: BreakpointObserver) {}

  ngOnInit(): void {
   
    this.breakpointObserver.observe([
      Breakpoints.XSmall,
      Breakpoints.Small,
      Breakpoints.Medium,
      Breakpoints.Large,
      Breakpoints.XLarge,
    ]).subscribe(result => {
      if (result.matches) {
        if (result.breakpoints[Breakpoints.XSmall]) {
          this.deviceType = 'xs';
          this.resolution = 'w:'+this.screenWidth+'  h:'+this.screenHeight;
        } else if (result.breakpoints[Breakpoints.Small]) {
          this.deviceType = 'sm';
          this.resolution = 'w:'+this.screenWidth+'  h:'+this.screenHeight;
        } else if (result.breakpoints[Breakpoints.Medium]) {
          this.deviceType = 'md';
          this.resolution = 'w:'+this.screenWidth+'  h:'+this.screenHeight;
        } else if (result.breakpoints[Breakpoints.Large]) {
          this.deviceType = 'lg';
          this.resolution = 'w:'+this.screenWidth+'  h:'+this.screenHeight;
        }else if (result.breakpoints[Breakpoints.XLarge]) {
          this.deviceType = 'xl';
          this.resolution = 'w:'+this.screenWidth+'  h:'+this.screenHeight;
        }else{
            this.deviceType = 'Unknown';
          this.resolution = 'w:'+this.screenWidth+'  h:'+this.screenHeight;
        }
      }
    });
    this.addNewItem( this.deviceType);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    this.screenWidth = event.target.innerWidth;
    this.screenHeight = event.target.innerHeight;
    this.resolution = 'w:'+this.screenWidth+'  h:'+this.screenHeight;
  }
  addNewItem(value: string) {
    this.newItemEvent.emit(value);
  }
}
