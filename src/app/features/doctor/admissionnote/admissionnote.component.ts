import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DateTime } from 'luxon';
import { DoctorService } from '../services/doctor.service';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzModalService } from 'ng-zorro-antd/modal';
import { NzCheckboxModule } from 'ng-zorro-antd/checkbox';



@Component({
  selector: 'app-admissionnote',
  templateUrl: './admissionnote.component.html',
  styleUrls: ['./admissionnote.component.css']
})
export class AdmissionnoteComponent {

  query: any = '';
  dataSet: any[] = [];
  loading = false;

  total = 0;
  pageSize = 20;
  pageIndex = 1;
  offset = 0;
  user_login_name: any;
  queryParamsData: any;
  patientList: any;
  hn: any;
  an: any;

  title: any;
  fname: any;
  lname: any;
  gender: any;
  age: any;
  address: any;
  phone: any;

  chief_complaint:any;
	present_illness:any;
  past_history:any;

  physical_exam:any;
  body_temperature:any;
      body_weight:any;
      body_height:any;
      waist:any;
      pulse_rate:any;
      respiratory_rate:any;
      systolic_blood_pressure:any;
      diatolic_blood_pressure:any;
      oxygen_sat:any;
      eye_score:any;
      movement_score:any;
      verbal_score:any;
bmi:any;


  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private doctorService: DoctorService,
    private message: NzMessageService,
    private modal: NzModalService,


  ) {

    let jsonString: any = this.activatedRoute.snapshot.queryParamMap.get('data');
    const jsonObject = JSON.parse(jsonString);
    this.queryParamsData = jsonObject;
    console.log(this.queryParamsData);

  }

  ngOnInit(): void {
    this.getList()

  }
  logOut() {
    sessionStorage.setItem('token', '');
    return this.router.navigate(['/login']);
  }

  onPageIndexChange(pageIndex: any) {

    this.offset = pageIndex === 1 ?
      0 : (pageIndex - 1) * this.pageSize;

    this.getList()
  }

  onPageSizeChange(pageSize: any) {
    this.pageSize = pageSize
    this.pageIndex = 1

    this.offset = 0

    this.getList()
  }

  async getList() {
    const messageId = this.message.loading('Loading...').messageId;
    try {
      const _limit = this.pageSize;
      const _offset = this.offset;
      const response = await this.doctorService.getPatientInfo(this.queryParamsData);

      const data: any = response.data;

      this.patientList = await data.data;
      console.log(this.patientList);
      this.hn = this.patientList.hn;
      this.an = this.patientList.an;
      this.title=this.patientList.title;
      this.fname=this.patientList.fname;
      this.lname=this.patientList.lname;
      this.gender=this.patientList.gender;
      this.age=this.patientList.age;
      this.address=this.patientList.address;
      this.phone=this.patientList.phone;
      this.chief_complaint=this.patientList.chief_complaint;
      this.present_illness=this.patientList.present_illness;
      this.past_history=this.patientList.past_history;
      this.physical_exam=this.patientList.physical_exam;
      this.body_temperature=this.patientList.body_temperature;
      this.body_weight=this.patientList.body_weight;
      this.body_height=this.patientList.body_height;
      this.waist=this.patientList.waist;
      this.pulse_rate=this.patientList.pulse_rate;
      this.respiratory_rate=this.patientList.respiratory_rate;
      this.systolic_blood_pressure=this.patientList.systolic_blood_pressure;
      this.diatolic_blood_pressure=this.patientList.diatolic_blood_pressure;
      this.oxygen_sat=this.patientList.oxygen_sat;
      this.eye_score=this.patientList.eye_score;
      this.movement_score=this.patientList.movement_score;
      this.verbal_score=this.patientList.verbal_score;
this.bmi=this.body_weight/((this.body_height/100)*(this.body_height/100));


      this.total = data.total || 1

      this.dataSet = data.data.map((v: any) => {
        const date = v.admit_date ? DateTime.fromISO(v.admit_date).setLocale('th').toLocaleString(DateTime.DATETIME_SHORT_WITH_SECONDS) : '';
        v.admit_date = date;
        return v;
      });
      this.message.remove(messageId);
    } catch (error: any) {
      this.message.remove(messageId);
      this.message.error(`${error.code} - ${error.message}`);
    }
  }
}

